// @ 是 src 目录的别名
import Home from '@/views/home'
import Express from '@/views/express'
import Edit from '@/views/edit'
import Receiver from '@/views/receiver'
import News from '@/views/news'
import Login from '../views/login'
import Register from '@/views/register'
import Mine from '@/views/mine'
import Freight from '@/views/freight'
import MyInfo from '@/views/myInfo'
import MyLocation from '@/views/myLocation'
import MySend from '@/views/mySend'
import RealName from '@/views/realName'
import Suggest from '@/views/suggest'

import Foot from '@/components/loginFooter'

// 路由路径的配置数组
const routes = [
  {
    path: '/',
    redirect: '/home' // 重定向
  },
  {
    path: '/home',
    name: 'Home', // 命名路由
    components: {
      default: Home
    }
  },
  {
    path: '/express',
    name: 'Express',
    components: {
      default: Express
    }
  },
  {
    path: '/edit',
    name: 'Edit',
    components: {
      default: Edit
    }
  },
  {
    path: '/receiver',
    name: 'Receiver',
    components: {
      default: Receiver
    }
  },
  {
    path: '/news',
    name: 'News',
    components: {
      default: News
    }
  },
  {
    path: '/login',
    name: 'Login',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    // component: () => import('../views/login')
    components: {
      footer: Foot,
      default: Login
    }
  },
  {
    path: '/register',
    name: 'Register',
    component: Register
  },
  {
    path: '/mine',
    name: 'Mine',
    component: Mine
  },
  {
    path: '/freight',
    name: 'Freight',
    component: Freight
  },

  {
    path: '/myinfo',
    name: 'MyInfo',
    component: MyInfo
  },
  {
    path: '/mylocation',
    name: 'MyLocation',
    component: MyLocation
  },
  {
    path: '/mysend',
    name: 'MySend',
    component: MySend
  },
  {
    path: '/realname',
    name: 'RealName',
    component: RealName
  },
  {
    path: '/suggest',
    name: 'Suggest',
    component: Suggest
  }
]

export default routes
