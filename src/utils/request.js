/* 实现 axios 二次封装 */
import axios from 'axios'
import { Toast } from 'vant'

// development: 开发环境  production：生产环境
// const baseURL = process.env.NODE_ENV === 'development' ? 'rap2' : 'real'
// const baseURL = process.env.NODE_ENV === 'development' ? 'rap2' : 'http://www.xiongmaoyouxuan.com/'

// 创建 axios 实例
const service = axios.create({
  baseURL: 'http://rap2api.taobao.org/app/mock/data',
  timeout: 3000
})

// 请求拦截
service.interceptors.request.use(config => {
  // 统一添加请求加载轻提示效果
  Toast.loading({
    message: '加载中...',
    forbidClick: true,
    duration: 0
  })

  // let token = null
  // if (localStorage.getItem('Token')) {
  //   // 自动登录
  //   token = localStorage.getItem('Token')
  //   console.log(JSON.parse(token).tele)
  // }
  // 向请求头中添加相关的 Header 头信息
  config.headers = {
    'X-Token': 'abc'
  }
  return config
})

// 响应拦截
service.interceptors.response.use(res => {
  // 关闭轻提示效果
  Toast.clear()
  // 对响应数据作处理
  if (res.status >= 200 && res.status < 300) {
    // 获取从后端接口中拿到的数据，该数据是前后端接口交互过程中的规范数据
    return res
    // 判断后端数据规范
  }

  // res.status 不以 2xx 开头，则说明请求可能有异常
  const e = new Error('API 请求异常')
  e.response = res
  return Promise.reject(e)
})

export default service
