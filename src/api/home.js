import request from '@/utils/request'

export const getHomePageData = () => {
  return request({
    url: '/1842355',
    method: 'GET'
  })
}
