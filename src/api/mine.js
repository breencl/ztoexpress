// http://rap2api.taobao.org/app/mock/data/1844272
import request from '@/utils/request'

export const getMineData = () => {
  return request({
    url: '/1844272',
    method: 'GET'
  })
}
