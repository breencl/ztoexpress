import request from '@/utils/request'

export const getNewsData = () => {
  return request({
    url: '/1844941',
    method: 'GET'
  })
}
