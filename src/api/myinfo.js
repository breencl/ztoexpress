// http://rap2api.taobao.org/app/mock/data/1844272
import request from '@/utils/request'

export const getMyInfo = () => {
  return request({
    url: '/1845501',
    method: 'GET'
  })
}
