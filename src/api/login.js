import request from '@/utils/request'

export const getLoginData = () => {
  return request({
    url: '/1842785',
    method: 'GET'
  })
}

export const getMoveCode = () => {
  return request({
    url: '/1842914',
    method: 'GET'
  })
}

export const getCode = () => {
  return request({
    url: '/1842914',
    method: 'GET'
  })
}
