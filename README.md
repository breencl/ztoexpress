# 中通快递手机端
## 简单接口
  1.首页(接口：rap2  链接：http://rap2api.taobao.org/app/mock/data/1842355)
  2.登录(接口：rap2  链接：http://rap2api.taobao.org/app/mock/data/1842785)
  3.注册
  4.寄件
  5.运费时效
  6.搜索(接口：百度  链接：https://www.baidu.com/sugrec?pre=1&p=3&ie=utf-8&json=1&prod=pc&from=pc_web&wd=acac)
## 页面描述
  1.首页：home
  2.登录：login
  3.注册：register
  4.寄件：express
  5.运费时效：fare
  