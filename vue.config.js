// vue.config.js 是 Vue CLI 的配置文件，会在启动项目时（开发环境下）或项目构建时自动加载该配置文件
module.exports = {
  devServer: {
    proxy: { // 反向代理
      '/zto': { // URL 以 '/zto' 开头的
        target: 'http://api.map.baidu.com',
        // 允许跨域
        changeOrigin: true,
        pathRewrite: {
          '^/zto': ''
        }
      }
    }
  },
  css: {
    loaderOptions: {
      less: {
        // 若 less-loader 版本小于 6.0，请移除 lessOptions 这一级，直接配置选项。
        modifyVars: {
          // 直接覆盖变量
          '@dropdown-menu-background-color': 'transparent',
          '@dropdown-menu-box-shadow': 'none',
          'stepper-input-font-size': '25px',
          '@stepper-input-text-color': '#63fff2',
          '@overlay-background-color': 'rgba(0, 0, 0, 0.4)'
        }
      }
    }
  }
  // devServer: {
  //   proxy: { // 反向代理
  //     '/zto': { // URL 以 '/zto' 开头的
  //       target: '',
  //       changeOrigin: true,
  //       pathRewrite: {
  //         '^/zto': ''
  //       }
  //     }
  //   }
  // }
}
